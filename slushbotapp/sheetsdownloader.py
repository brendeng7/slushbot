#from __future__ import print_function
import httplib2
import os
import time

from apiclient import discovery
from apiclient import errors
from apiclient import http
import oauth2client
from oauth2client import client
from oauth2client import tools


class SheetsDownloader:
    SCOPES=None
    CLIENT_SECRET_FILE=None
    APPLICATION_NAME=None
    flags=None
    def __init__(self):

        try:
            import argparse
            self.flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
        except ImportError:
            self.flags = None

        self.SCOPES = 'https://www.googleapis.com/auth/drive'
        self.CLIENT_SECRET_FILE = 'client_secret.json'
        self.APPLICATION_NAME = 'slushbot'

    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'drive-python-quickstart.json')

        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
            flow.user_agent = self.APPLICATION_NAME
            if self.flags:
                credentials = tools.run_flow(flow, store, self.flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print 'Storing credentials to ' + credential_path
        
        return credentials

    def download_sheet(self):
        """Download a file's content.

          Args:
          service: Drive API service instance.
          drive_file: Drive File instance.

          Returns:
          File's content if successful, None otherwise.
        """
        credentials = self.get_credentials()
        http_arg = credentials.authorize(httplib2.Http())
        
        service = discovery.build('drive', 'v2', http=http_arg)
        file_id='1G4bdlIs059ogEfhOV9vbJbGdYQjcbOXHTKu8n0iXjxU'
        
   

        filename='slush_spring_16'+time.strftime("_%d-%m-%Y_%H:%M:%S")+'.xlsx'

        drive_file = service.files().get(fileId=file_id).execute()      

        download_url = drive_file['exportLinks']['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
        if download_url:
            resp, content = service._http.request(download_url)
            if resp.status == 200:
                #print ('Status: %s' % resp)
                f = open(filename,"w")
                f.write(content)
                return filename
            else:
                print ('An error occurred: %s' % resp)
                return None
        else:
            return None
        

