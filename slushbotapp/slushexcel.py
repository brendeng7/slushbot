

class SlushExcel:
    total_income=0
    total_expenses=0
    balance=0
    used=0
    inventory_val=0
    inventory_last_updated=0

    inventory_headers=[]
    transaction_headers=[]
    income=[]
    expenses=[]
    inventory={}

    def __init__(self):

      pass

    def set_main_values(self, args):
        self.balance=args['balance']
        self.used=args['used']
        self.inventory_val=args['inventory_val']
        self.total_income=args['total_income']
        self.total_expenses=args['total_expenses']

        pass

    #income and expenses are dictionaries as follows
    #{id: , date:, person:, description:, receipt:}

    def set_transaction_headers(self, arg):
        self.transaction_headers=arg
        pass

    def set_inventory_headers(self, arg):
        self.inventory_headers=arg
        pass

    def add_income(self, income_item):
        self.income.append(income_item)
        pass
    
    def add_expense(self, expense_item):
        self.expenses.append(expense_item)
        pass
    
    def add_inventory(self, key, inventory_item):
        self.inventory[key]=inventory_item
        pass
    
    def set_inventory_updated(self, arg):
        self.inventory_last_updated=arg

    def print_full(self):
    
        pass
    
    def print_book(self):
        print self.total_income, self.total_expenses, self.balance, self.inventory_val, self.used
        print self.inventory_last_updated
        print self.transaction_headers
        print self.inventory_headers
        for x in self.income:
            print x
        for x in self.expenses:
            print x
        for x,y in self.inventory.items():
            print x, y
        pass

    def print_inventory(self):
        
        pass





