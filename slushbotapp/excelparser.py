#This class can be used to parse an excel sheet 
import openpyxl 
import slushexcel as se

class ExcelParser:
    
    def __init__(self):

        pass


    def create_slushbook(self, file_name):

        slushbook=se.SlushExcel()

        wb = openpyxl.load_workbook(filename=file_name, data_only=True)

        book_ws=wb.get_sheet_by_name('Book')
        inventory_ws=wb.get_sheet_by_name('Inventory')


        self.parse_book(book_ws, slushbook)
        self.parse_inventory(inventory_ws, slushbook)

        return slushbook

    def parse_book(self, ws, slushbook):
        items = False
        expense_start_index=0
        main_values_arg={}

        for row in ws.iter_rows():
            cell=row[0]
            
            if items:
                if cell.value!=None:
                    income_dict={}
                    for i,key in enumerate(headers):
                        income_dict[key]=str(row[i].value)
                    slushbook.add_income(income_dict)

                if row[expense_start_index].value!=None:
                    expense_dict={}
                    for i,key in enumerate(headers):
                        expense_dict[key]=str(row[expense_start_index+i].value)
                    slushbook.add_expense(expense_dict)

            else:
                label = str(cell.value)
                try:
                    val = float(str(row[1].internal_value))
                except ValueError:
                    val=0

                if label=='Balance':
                    main_values_arg['balance']=val
                    pass
                elif label=='Inventory':
                    main_values_arg['inventory_val']=val
                    pass
                elif label=='Used':
                    main_values_arg['used']=val
                    pass 
                elif label=='Income' and (not 'total_income' in main_values_arg):
                    main_values_arg['total_income']=val
                    pass
                elif label=='Expenses':
                    main_values_arg['total_expenses']=val
                elif label=='ID':
                    slushbook.set_main_values(main_values_arg)
                    headers=['ID']
                    items=True
                    for i in range(1,len(row)):
                        if str(row[i].value)=='ID':
                            expense_start_index=i
                            break
                        else:
                            headers.append(str(row[i].value))
                    slushbook.set_transaction_headers(headers)
                else:
                    pass    
        pass

    def parse_inventory(self, ws, slushbook):
        entries=False
        for row in ws.iter_rows():
            cell=row[0]
            if entries: #Currently parsing the inventory rows
                if cell.value!=None:
                    inv_key=str(cell.value)
                    inventory_dict={}
                    for i,key in enumerate(headers):
                        inventory_dict[key]=str(row[i].value)
                    slushbook.add_inventory(inv_key,inventory_dict)

            else:
                #Currently parsing information at top of workbook
                row_text=str(row[0].value)
                if row_text=="Inventory Last Updated":
                    slushbook.set_inventory_updated(str(row[1].value))
                elif row_text=='UPC':
                    entries=True
                    headers=['UPC']
                    for i in range(1,len(row)):
                        if row[i].value==None:
                            break
                        headers.append(str(row[i].value))
                    slushbook.set_inventory_headers(headers)
        pass